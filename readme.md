# check if poetry.lock update to date

```yaml
  - repo: https://github.com/Trim21/poetry-check-lock.git
    rev: v0.0.3
    hooks:
      - id: poetry-check-lock
```
